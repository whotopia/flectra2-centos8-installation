#!/bin/bash -
#title          :install-flectra2-centos8.sh
#description    :Install Flectra 2.0 completely on a fresh installation of CentOS 8 including PostgreSQL.
#                  The installation makes use of Flectra user account and Python3 virtual environment.
#author         :Steven Uggowitzer
#date           :20201225
#version        :1.0
#usage          :./install-flectra2-centos8.sh
#notes          : Generally you can run this script more than once and it shouldn't break anything.
#notes          :   2020/12/25 -- currently there is an issue with CentOS 8 upgrading the filesystem package.  Please ignore errors till this is fixed.
#bash_version   :4.4.19(1)-release
#============================================================================

APPADMINPWD=yourhappyflectra2password!
APP=flectra
APPURL=https://gitlab.com/flectra-hq/flectra.git
APPMAJVER=2
APPMINVER=0
APPPORT=8069
DEBUG=0

# wkhtmltopdf Filename and URL
WKFILE=wkhtmltox-0.12.6-1.centos8.x86_64.rpm
WKURL=https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/$WKFILE

# Raven package and URL
RAVENURL=https://pkgs.dyn.su/el8/base/x86_64/raven-release-1.0-1.el8.noarch.rpm
RAVENPKG=raven-release


#============================================================================
APPVER=$APPMAJVER.$APPMINVER
APPNAME=$APP$APPVER
APPUSERDIR=/opt/$APPNAME
APPID=$APP$APPMAJVER
APPDIR=$APPUSERDIR/$APPID
APPUSER=$APPID

echo $APP -- APPNAME: $APPNAME
echo $APP -- APPUSERDIR.....: $APPUSERDIR
echo $APP -- APPID .........: $APPID
echo $APP -- APPDIR ........: $APPDIR
echo $APP -- APPUSER .......: $APPUSER

if [ ! -f /etc/centos-release ]; then
  echo This script is not running under CentOS.  Exiting.
  exit
else
  echo CentOS found.
fi

full=`cat /etc/centos-release | tr -dc '0-9.'`
major=$(cat /etc/centos-release | tr -dc '0-9.'|cut -d \. -f1)
minor=$(cat /etc/centos-release | tr -dc '0-9.'|cut -d \. -f2)
asynchronous=$(cat /etc/centos-release | tr -dc '0-9.'|cut -d \. -f3)

if [[ "$major" != 8 ]]; then
  echo Incorrect CentOS version. Exiting.
  exit
 else
  echo CentOS 8 detected. Continuing.
fi

echo CentOS Version: $full
echo Major Relase: $major
echo Minor Relase: $minor
echo Asynchronous Relase: $asynchronous
echo Installing $APPNAME Version $APPVER to indentifier $APPID

dnf -y update
dnf -y install openssh-server

systemctl enable sshd
systemctl start sshd

dnf -y install epel-release
dnf -y install dnf-plugins-core
dnf config-manager --set-enabled powertools

#Install Raven as may be required in base Python for some versions
echo "rpm -i $RAVENURL"
rpm -i $RAVENURL
echo "dnf -y install $RAVENPKG"
dnf -y install $RAVENPKG

# Install some packages that I need or like to have, or that Flectra actually requires.
dnf -y update
dnf -y upgrade

dnf -y install python3 python3-devel python3-virtualenv
dnf -y install git gcc wget curl htop
dnf -y install nodejs libxslt-devel bzip2-devel openldap-devel libjpeg-devel freetype-devel
dnf -y install zlib-devel openssl-devel ncurses-devel
dnf -y install sqlite-devel readline-devel tk-devel
dnf -y install gdbm-devel db4-devel libffi-devel
dnf -y install libxslt libxslt-devel libxml2 libxml2-devel
dnf -y install openldap-devel libjpeg-turbo-devel libtiff-devel libyaml
dnf -y install git libpng12
dnf -y install libXext xorg-x11-fonts-Type1

#needed by Flectra
npm install -g less
npm install -g less-plugin-clean-css


# Create user as required.
id -u $APPUSER &>/dev/null ||    useradd -m -U -r -d $APPUSERDIR -s /bin/bash $APPUSER

#===================================================
#==Start - Installation of PostgreSQL
# PostgreSQL Database installation of whatever is available in the CentOS Distro
#===================================================
dnf -y install postgresql postgresql-server postgresql-contrib
## Initialize the database
if [ ! -f /var/lib/pgsql/data/PG_VERSION ]; then
   echo "Initializing PG instance"
   /usr/bin/postgresql-setup initdb
fi
systemctl start postgresql
systemctl enable postgresql

### Note older versions of Odoo need the base PostgreSQL template with
# UTF8 need templates to be compatible.  So doing that here.
su - postgres -c psql postgres <<EOF
update pg_database set datallowconn = TRUE where datname = 'template0';
\c template0
update pg_database set datistemplate = FALSE where datname = 'template1';
drop database template1;
create database template1 with template = template0 encoding = 'UTF8';
update pg_database set datistemplate = TRUE where datname = 'template1';
\c template1
update pg_database set datallowconn = FALSE where datname = 'template0';
EOF

### flectra needs a user '$APPUSER' in the system for database connections to work
su - postgres -c "createuser -s $APPUSER"
#===================================================


#===================================================
### Install the wkhtmltopdf applications
#===================================================
if [ ! -f /opt/$WKFILE ]; then
  cd /opt/ && wget $WKURL
fi
dnf -y localinstall /opt/$WKFILE
### For Odoo/Flectra perhaps use the MS Fonts for Reporting (Odoo8 reporting and perhaps others)
dnf -y install libmspack cabextract
rpm -ivh https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm
dnf -y update
dnf -y upgrade
#===================================================


#===================================================
### As the App (Flectra)  user
#===================================================
su - $APPUSER <<EOF
  echo Current user: $USER
  echo Cloning $APPURL with branch $APPVER to $APPDIR
  git clone $APPURL --depth 1 --branch $APPVER $APPDIR

  cd $APPUSERDIR && virtualenv-3 -p /usr/bin/python3 $APPID-venv
  source $APPID-venv/bin/activate

  pip install -r $APPID/requirements.txt
  cd $APPDIR
  pip install ./
  pip install --upgrade pip

  #needed for modern CentOS lest things fail
  pip install psycopg2-binary

  #needed
  pip install lessc

  deactivate

  mkdir -p $APPDIR-local
EOF

# Create a custom addon directory
mkdir -p $APPDIR-custom-addons
chown $APPUSER: $APPDIR-custom-addons

# Store logs in standard /var/log location
mkdir /var/log/$APPID && touch /var/log/$APPID/$APP.log
chown -R $APPUSER: /var/log/$APPID/


#===================================================
### Write the Flectra configuration file to userdir
#===================================================

cat <<EOF > /$APPUSERDIR/$APP.conf
[options]
; This is the password that allows database operations:
; admin_passwd = admin
admin_passwd=$APPADMINPWD
db_host = False
db_port = False
db_user = $APPUSER
db_password = False
xmlrpc_port = $APPPORT
; longpolling_port = 8072
logfile = /var/log/$APPID/$APP.log
logrotate = True
addons_path = $APPDIR/addons,$APPDIR-custom-addons
session_dir = $APPDIR-local
data_dir = $APPDIR-local
EOF

chown -R $APPUSER: /$APPUSERDIR/$APP.conf

#===================================================
### Install the systemd script to start the service
#===================================================
cat <<EOF > /etc/systemd/system/$APPID.service
[Unit]
Description=$APPNAME
#Requires= postgresql.service
#After=network.target postgresql.service
[Service]
Type=simple
SyslogIdentifier=$APPID
PermissionsStartOnly=true
User=$APPUSER
Group=$APPUSER
ExecStart=$APPDIR-venv/bin/python3 $APPDIR/$APP-bin -c $APPUSERDIR/$APP.conf
StandardOutput=journal+console
[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl start $APPID
systemctl enable $APPID
systemctl status $APPID.service &

echo  Youl should be able to visit $APPID now on http://IP-ADDRESS:$APPPORT/
echo  --- Done.  $APPID services should be started.
